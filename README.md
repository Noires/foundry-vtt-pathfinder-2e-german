# Foundry VTT Pathfinder 2e German Translation

German language support for Pathfinder 2e in Foundry VTT (https://gitlab.com/hooking/foundry-vtt---pathfinder-2e).

This translations uses trademarks and/or copyrights owned by Paizo Inc., which are used under Paizo's Community Use Policy. We are expressly prohibited from charging you to use or access this content. This system is not published, endorsed, or specifically approved by Paizo Inc. For more information about Paizo's Community Use Policy, please visit paizo.com/communityuse. For more information about Paizo Inc. and Paizo products, please visit paizo.com.

## Installation

1. Copy this link and use it in the Foundrys module manager to install the module. 
    > https://gitlab.com/Noires/foundry-vtt-pathfinder-2e-german/-/raw/master/module.json
2. Enable the module in your own worlds module settings.

